import Firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyAoAkIVeeBP53FcavjEtOtVL8-T1WQ1yk4',
  authDomain: 'nlt-portfolio-console.firebaseapp.com',
  databaseURL: 'https://nlt-portfolio-console.firebaseio.com',
  projectId: 'nlt-portfolio-console',
  storageBucket: 'nlt-portfolio-console.appspot.com',
  messagingSenderId: '584701864950'
}

const app = Firebase.initializeApp(config)

export default app
