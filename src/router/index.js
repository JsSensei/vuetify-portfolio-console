import Vue from 'vue'
import Router from 'vue-router'
import LoginComponent from '@/components/LoginComponent'
import ConsoleComponent from '@/components/ConsoleComponent'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LoginComponent',
      component: LoginComponent
    },
    {
      path: '/console',
      name: 'ConsoleComponent',
      component: ConsoleComponent
    }
  ]
})
